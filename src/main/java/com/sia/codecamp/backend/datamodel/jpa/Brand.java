package com.sia.codecamp.backend.datamodel.jpa;

import com.google.gson.Gson;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Setter
@Getter
@AllArgsConstructor
public class Brand implements Serializable {

    private String brandID;
    private String brandName;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;
    private Map<String, Product> products;

    public Brand() {
        this.products = new ConcurrentHashMap<>();
    }

    public Brand(String brandID, String brandName, LocalDateTime createdDate, LocalDateTime updatedDate) {
        this.brandID = brandID;
        this.brandName = brandName;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.products = new ConcurrentHashMap<>();
    }

    public void addProduct(Product entity){
        this.products.put(entity.getProductName(), entity);
    }

    public void removeProduct(String productName){
        this.products.remove(productName);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
