package com.sia.codecamp.backend.service;

import com.google.gson.Gson;
import com.sia.codecamp.backend.datamodel.jpa.AuditLog;
import com.sia.codecamp.backend.datamodel.jpa.Brand;
import com.sia.codecamp.backend.datamodel.jpa.Product;
import com.sia.codecamp.backend.datamodel.rest.AuditDTO;
import com.sia.codecamp.backend.datamodel.rest.GraphDataDTO;
import com.sia.codecamp.backend.datamodel.rest.ProductDTO;
import com.sia.codecamp.backend.datamodel.rest.RequestData;
import com.sia.codecamp.backend.enumeration.SystemEnum;
import com.sia.codecamp.backend.error.InsufficientStockError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class RedisService {

    private static final String BRANDHASH = "codecamo.brand";
    private static final String ORDERHASH = "codecamp.order.history";

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss:SSS");

    @Autowired
    private ReactiveHashOperations<String, String, String> hashOperations;

    @Autowired
    private CommonService common;

    public Mono<ProductDTO> updateStock(RequestData requestData, boolean restock){
        AuditLog auditLog = new AuditLog(UUID.randomUUID().toString(), SystemEnum.ActionType.PURCHASE, requestData.getBrandName(), requestData.getProductName(), LocalDateTime.now());
        return hashOperations.get(BRANDHASH, requestData.getBrandName())
                .log()
                .map(s -> common.genericMapper(s, Brand.class))
                .flatMap(s -> {
                    Product product = s.getProducts().get(requestData.getProductName());
                    auditLog.setCurrentStockAmount(product.getStockCount());
                    if(restock){
                        int newAmount = product.getStockCount() + requestData.getAmount();
                        product.setStockCount(newAmount);
                    }
                    else {
                        int newAmount = product.getStockCount() - requestData.getAmount();
                        if (newAmount >= 0) {
                            product.setStockCount(newAmount);
                            log.info("Requested stock is ok! Moving on.");
                        } else {
                            log.info("Requested amount is more than available stock!");
                            return Mono.error(new InsufficientStockError("Sorry, requested amount is too much."));
                        }
                    }
                    s.getProducts().replace(requestData.getProductName(), product);
                    auditLog.setNewStockAmount(product.getStockCount());
                    auditLog.setChangedAmount(auditLog.getCurrentStockAmount() - auditLog.getNewStockAmount());
                    return Mono.just(s);
                })
                .flatMap(s -> hashOperations.put(BRANDHASH, requestData.getBrandName(), s.toString()))
                .doOnSuccess(s -> {
                    saveOrderHistory(auditLog);
                })
                .flatMap(s -> retrieveProduct(requestData.getBrandName(), requestData.getProductName()))
                .switchIfEmpty(Mono.empty())
                .onErrorResume(e -> Mono.error(new InsufficientStockError("Insufficient Stock for requested Product.")));
    }

    public Mono<Boolean> hasEnoughStock(String brand, String product, int requiredAmount){
        return hashOperations.get(BRANDHASH, brand)
                .map(s -> common.genericMapper(s, Brand.class))
                .flatMap(s -> {
                    return Mono.just(s.getProducts().get(product));
                })
                .flatMap(s -> {
                    if(s.getStockCount() >= requiredAmount)
                        return Mono.just(true);
                    else
                        return Mono.just(false);
                });
    }

    public Mono<ProductDTO> retrieveProduct(String brandName, String productName){
        return hashOperations.get(BRANDHASH, brandName)
                .map(s -> common.genericMapper(s, Brand.class))
                .flatMap(s -> {
                    Product product = s.getProducts().get(productName);
                    return Mono.just(new ProductDTO(s.getBrandName(), product.getProductName(), product.getStockCount(), product.getCreatedDate(), product.getUpdatedDate(), LocalDateTime.now()));
                });
    }

    public void saveOrderHistory(AuditLog auditLog){
        hashOperations.put(ORDERHASH, auditLog.getAuditID(), auditLog.toString()).subscribe(s -> log.info("AuditLog has been saved into db. {}", auditLog));
    }

    public Mono<List<GraphDataDTO>> retrieveHistoryGraph(){
        List<GraphDataDTO> graphDataDTOS = new ArrayList<>();
        for (SystemEnum.BrandType brandType : SystemEnum.BrandType.values()) {
            GraphDataDTO graphDataDTO = new GraphDataDTO();
            graphDataDTO.setBrandName(brandType.getName());
            hashOperations.values(ORDERHASH)
                    .map(s -> common.genericMapper(s, AuditLog.class))
                    .subscribe(s -> {
                        if(s.getBrandName().equals(brandType.getName())){
                            graphDataDTO.addStockGraph(s.getTransactionDate().format(formatter), s.getCurrentStockAmount());

                        }
                    });
            graphDataDTOS.add(graphDataDTO);
        }
        return Mono.just(graphDataDTOS);
    }


//    @EventListener(ApplicationReadyEvent.class)
//    public void findData(){
//
//        hashOperations.keys(BRANDHASH)
//                .collectList()
//                .subscribe(s -> {
//                    if(s.size() == 0 ){
//                        for (SystemEnum.BrandType brandType : SystemEnum.BrandType.values()) {
//                            Brand brand = new Brand(UUID.randomUUID().toString(), brandType.getName(), LocalDateTime.now(), LocalDateTime.now());
//                            for (SystemEnum.ProductType productType : SystemEnum.ProductType.values()) {
//                                brand.addProduct(new Product(UUID.randomUUID().toString(), productType.getName(), 10, LocalDateTime.now(), LocalDateTime.now()));
//                            }
//                            hashOperations.put(BRANDHASH, brandType.getName(), brand.toString()).subscribe();
//                        }
//                        log.info("Loaded starter data into redis.");
//                    }
//                });
//
//        RequestData data = new RequestData("Pilot","Pencil", 2);
//        updateStock(data, false);
////        hashOperations.get(BRANDHASH, data.getBrandName())
////                .map(s -> genericMapper(s, Brand.class))
////                .subscribe(s -> {
////                    log.info("Phase 1 Retrieval: {}",s);
////                });
//    }


}
