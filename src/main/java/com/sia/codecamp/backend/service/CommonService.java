package com.sia.codecamp.backend.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class CommonService {

    @Autowired
    private Gson gson;

    public <T> T genericMapper(String data, Class<T> mapToClass){
        return gson.fromJson(data, mapToClass);
    }


}
