package com.sia.codecamp.backend;

import com.sia.codecamp.backend.datamodel.rest.RequestData;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import redis.embedded.RedisServer;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@AutoConfigureWebTestClient
public class APIControllerTests {

    @Autowired
    private WebTestClient client;


    @Test
    public void orderAPI_Success() {
        RequestData data = new RequestData("Pentel", "Pen", 1);
        client.post().uri("/order/request")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(data), RequestData.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody();
    }

    @Test
    public void orderAPI_Failure_InsufficientStock(){
        RequestData data = new RequestData("Pentel", "Pen", 9999);
        client.post().uri("/order/request")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(data), RequestData.class)
                .exchange()
                .expectStatus().is5xxServerError();
    }

    @Test
    public void orderAPI_Failure_NonExistingProductOrBrand(){
        RequestData data = new RequestData("Toyota", "Vios", 1);
        client.post().uri("/order/request")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(data), RequestData.class)
                .exchange()
                .expectStatus().is4xxClientError();
    }


}
