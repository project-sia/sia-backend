package com.sia.codecamp.backend.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ProductValidation.class)
@Target({ ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ProductValidCheck {

    String message() default "Invalid Product Type.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
