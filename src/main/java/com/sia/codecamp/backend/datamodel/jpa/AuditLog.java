package com.sia.codecamp.backend.datamodel.jpa;

import com.google.gson.Gson;
import com.sia.codecamp.backend.enumeration.SystemEnum;
import lombok.*;

import javax.persistence.*;
import javax.swing.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuditLog implements Serializable {
    private String auditID;
    private SystemEnum.ActionType actionType;
    private String brandName;
    private String productName;
    private int currentStockAmount;
    private int newStockAmount;
    private int changedAmount;
    private LocalDateTime transactionDate;

    public AuditLog(String auditID, SystemEnum.ActionType actionType, String brandName, String productName, LocalDateTime transactionDate) {
        this.auditID = auditID;
        this.actionType = actionType;
        this.brandName = brandName;
        this.productName = productName;
        this.transactionDate = transactionDate;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
