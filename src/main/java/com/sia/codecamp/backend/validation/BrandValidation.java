package com.sia.codecamp.backend.validation;

import com.sia.codecamp.backend.enumeration.SystemEnum;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class BrandValidation implements ConstraintValidator<BrandValidCheck, String> {
    @Override
    public void initialize(BrandValidCheck constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        List<SystemEnum.BrandType> brands = Arrays.asList(SystemEnum.BrandType.values());
        log.info("{}", brands);
        log.info("Input: {}", s);
        for (SystemEnum.BrandType brand : brands) {
            if(brand.getName().equalsIgnoreCase(s))
                return true;
        }

        return false;

    }
}
