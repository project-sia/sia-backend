package com.sia.codecamp.backend.validation;

import com.sia.codecamp.backend.enumeration.SystemEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class ProductValidation implements ConstraintValidator<ProductValidCheck, String> {
    @Override
    public void initialize(ProductValidCheck constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        List<SystemEnum.ProductType> products = Arrays.asList(SystemEnum.ProductType.values());

        for (SystemEnum.ProductType product : products) {
            if(product.getName().equalsIgnoreCase(s))
                return true;
        }

        return false;
    }
}
