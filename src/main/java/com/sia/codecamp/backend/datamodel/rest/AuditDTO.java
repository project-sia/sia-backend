package com.sia.codecamp.backend.datamodel.rest;

import com.google.gson.Gson;
import com.sia.codecamp.backend.enumeration.SystemEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuditDTO {

    private String auditID;
    private SystemEnum.ActionType actionType;
    private String brandName;
    private String productName;
    private int stockAmount;
    private int changedAmount;
    private Date transactionDate;
    private Date requestDate;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
