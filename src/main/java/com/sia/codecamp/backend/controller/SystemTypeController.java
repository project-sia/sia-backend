package com.sia.codecamp.backend.controller;

import com.sia.codecamp.backend.datamodel.rest.BrandProductDTO;
import com.sia.codecamp.backend.enumeration.SystemEnum;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/system")
public class SystemTypeController {

    @GetMapping("/type")
    public Mono<ResponseEntity> getBrandAndProductType(){
        return Mono.just(new BrandProductDTO())
                .flatMap(s -> {
                    List<String> products = new ArrayList<>();
                    for (SystemEnum.ProductType productType : SystemEnum.ProductType.values()) {
                        products.add(productType.getName());
                    }
                    s.setProducts(products);
                    return Mono.just(s);
                })
                .flatMap(s -> {
                    List<String> brands = new ArrayList<>();
                    for (SystemEnum.BrandType brandType : SystemEnum.BrandType.values()) {
                        brands.add(brandType.getName());
                    }
                    s.setBrands(brands);
                    return Mono.just(s);
                })
                .flatMap(s -> Mono.just(ok(s)));
    }
}
