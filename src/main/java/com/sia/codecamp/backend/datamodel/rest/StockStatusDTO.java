package com.sia.codecamp.backend.datamodel.rest;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StockStatusDTO {

    private String brand;
    private List<StockAmountDTO> stockStatuses;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
