package com.sia.codecamp.backend.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BrandValidation.class)
@Target({ ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface BrandValidCheck {

    String message() default "Invalid Brand Type.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
