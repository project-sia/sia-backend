package com.sia.codecamp.backend.datamodel.rest;

import com.google.gson.Gson;
import com.sia.codecamp.backend.validation.BrandValidCheck;
import com.sia.codecamp.backend.validation.ProductValidCheck;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestData {

    @NotEmpty
    @BrandValidCheck
    private String brandName;
    @NotEmpty
    @ProductValidCheck
    private String productName;
    @Min(1)
    private int amount;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
