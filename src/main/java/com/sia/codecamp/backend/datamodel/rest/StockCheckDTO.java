package com.sia.codecamp.backend.datamodel.rest;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StockCheckDTO {

    private boolean stockStatus;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
