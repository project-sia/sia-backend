package com.sia.codecamp.backend.datamodel.rest;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
@AllArgsConstructor
public class GraphDataDTO {

    String brandName;
    Map<String, Integer> stockGraphData;

    public GraphDataDTO() {
        stockGraphData = new ConcurrentHashMap<>();
    }

    public void addStockGraph(String key, int amount){
        stockGraphData.put(key, amount);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
