package com.sia.codecamp.backend;

import com.sia.codecamp.backend.datamodel.jpa.Brand;
import com.sia.codecamp.backend.datamodel.jpa.Product;
import com.sia.codecamp.backend.datamodel.rest.RequestData;
import com.sia.codecamp.backend.enumeration.SystemEnum;
import com.sia.codecamp.backend.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.stereotype.Component;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Component
@Slf4j
public class EmbeddedRedis {



    @Value("${spring.redis.port}")
    private int redisPort;
    private RedisServer redisServer;

    private static final String BRANDHASH = "codecamo.brand";
    private static final String ORDERHASH = "codecamp.order.history";

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss:SSS");

    @Autowired
    private ReactiveHashOperations<String, String, String> hashOperations;

    @Autowired
    private RedisService redisService;

    @PostConstruct
    public void startServer() throws IOException {
        redisServer = new RedisServer(redisPort);
        redisServer.start();
    }

    @PreDestroy
    public void stopServer() throws IOException {
        redisServer.stop();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void findData(){

        hashOperations.keys(BRANDHASH)
                .collectList()
                .subscribe(s -> {
                    if(s.size() == 0 ){
                        for (SystemEnum.BrandType brandType : SystemEnum.BrandType.values()) {
                            Brand brand = new Brand(UUID.randomUUID().toString(), brandType.getName(), LocalDateTime.now(), LocalDateTime.now());
                            for (SystemEnum.ProductType productType : SystemEnum.ProductType.values()) {
                                brand.addProduct(new Product(UUID.randomUUID().toString(), productType.getName(), 10, LocalDateTime.now(), LocalDateTime.now()));
                            }
                            hashOperations.put(BRANDHASH, brandType.getName(), brand.toString()).subscribe();
                        }
                        log.info("Loaded starter data into redis.");
                    }
                });

        RequestData data = new RequestData("Pilot","Pencil", 2);
        redisService.updateStock(data, false);
    }
}
