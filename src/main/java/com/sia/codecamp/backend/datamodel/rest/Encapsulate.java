package com.sia.codecamp.backend.datamodel.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Encapsulate {

    private String status;
    private String message;
    private Object data;

    public Encapsulate(String status, String message) {
        this.status = status;
        this.message = message;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
