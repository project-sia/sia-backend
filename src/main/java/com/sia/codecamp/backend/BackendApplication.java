package com.sia.codecamp.backend;

import com.sia.codecamp.backend.datamodel.jpa.Brand;
import com.sia.codecamp.backend.datamodel.jpa.Product;
import com.sia.codecamp.backend.enumeration.SystemEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class BackendApplication {


    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }



}

