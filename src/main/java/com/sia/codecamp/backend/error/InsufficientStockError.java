package com.sia.codecamp.backend.error;

public class InsufficientStockError extends RuntimeException {
    public InsufficientStockError() {
        super();
    }

    public InsufficientStockError(String message) {
        super(message);
    }
}
