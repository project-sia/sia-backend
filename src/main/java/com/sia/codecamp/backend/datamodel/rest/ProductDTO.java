package com.sia.codecamp.backend.datamodel.rest;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {

    private String brandName;
    private String productName;
    private int stockCount;
    private LocalDateTime createdTimestamp;
    private LocalDateTime lastUpdateTimestamp;
    private LocalDateTime retrievedTimestamp;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
