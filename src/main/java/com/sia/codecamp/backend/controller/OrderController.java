package com.sia.codecamp.backend.controller;

import com.sia.codecamp.backend.datamodel.rest.Encapsulate;
import com.sia.codecamp.backend.datamodel.rest.RequestData;
import com.sia.codecamp.backend.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/order")
@CrossOrigin("http://localhost:3000")
@Slf4j
public class OrderController {

    @Autowired
    private RedisService redisService;

    @PostMapping("/request")
    public Mono<ResponseEntity<Encapsulate>> orderRequest(@RequestBody @Validated RequestData request) {

        return redisService.updateStock(request, false)
                .flatMap(s -> Mono.just(ok(new Encapsulate("Success", "Request Executed Properly", s))))
                .onErrorReturn(ResponseEntity.status(500).body(new Encapsulate("Failure", String.format("Not enough stock for requested Product %s under Brand %s.",
                        request.getProductName(),  request.getBrandName()))))
                .switchIfEmpty(Mono.just(status(404).body(new Encapsulate("Failed", "Couldn't find requested Brand and Product"))));
    }

    @GetMapping("/request")
    public Mono<ResponseEntity> stockStatus(@RequestParam String brand, @RequestParam String product){
        log.info("{}, {}", brand, product);
        return redisService.retrieveProduct(brand, product)
                .flatMap(s -> Mono.just(ok(s)));
    }

    @GetMapping("/graphHistory")
    public Mono<ResponseEntity> graphHistory(){
        return redisService.retrieveHistoryGraph()
                .flatMap(s -> Mono.just(ok(s)));
    }
//
//    @GetMapping("/request")
//    public Mono<ResponseEntity> orderHistory(){
//
//    }

}
