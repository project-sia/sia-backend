package com.sia.codecamp.backend.enumeration;

import lombok.Getter;

public class SystemEnum {

    @Getter
    public static enum BrandType {
        PILOT("Pilot"),
        UNI("Uni"),
        ZEBRA("Zebra"),
        PENTEL("Pentel"),
        STAEDTLER("Staedtler"),
        PAPERMATE("PaperMate");


        private String name;
        BrandType(String name){
            this.name = name;
        }


    }

    @Getter
    public static enum ProductType {
        PEN("Pen"),
        Pencil("Pencil"),
        ERASER("Eraser"),
        WHITEOUT("Whiteout");

        private String name;
        ProductType(String name){
            this.name = name;
        }
    }

    @Getter
    public static enum ActionType {
        SEARCH("Search"),
        PURCHASE("Purchase"),
        RESTOCK("Restock"),
        BATCHUPDATE("Batch Update"),
        BATCHPURCHASE("Batch Purchase");

        private String name;
        ActionType(String name){
            this.name = name;
        }
    }
}
