package com.sia.codecamp.backend.datamodel.rest;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StockAmountDTO {

    private String product;
    private int stockAmount;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
