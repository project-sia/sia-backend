package com.sia.codecamp.backend.controller;

import com.sia.codecamp.backend.datamodel.rest.ProductDTO;
import com.sia.codecamp.backend.datamodel.rest.RequestData;
import com.sia.codecamp.backend.datamodel.rest.StockCheckDTO;
import com.sia.codecamp.backend.service.CommonService;
import com.sia.codecamp.backend.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/stock")
@CrossOrigin("http://localhost:3000")
@Slf4j
public class StockController {

    @Autowired
    private RedisService redisService;


    @GetMapping("/status")
    public Mono<ResponseEntity> stockStatusAPI(){
        return Mono.just(ok(new ProductDTO()));
    }

    @PostMapping("/stockCheck")
    public Mono<ResponseEntity> stockCheckAPI(@Validated @RequestBody RequestData request){
        return redisService.hasEnoughStock(request.getBrandName(), request.getProductName(), request.getAmount())
                .flatMap(s -> Mono.just(ok(new StockCheckDTO(s))));
    }

    @PatchMapping("/request")
    public Mono<ResponseEntity> stockUpdateAPI(@RequestBody @Validated RequestData request){
        log.info("API Request :: Stock Update :: {}", request);
        return redisService.updateStock(request, true)
                .flatMap(s -> Mono.just(ok(s)));
    }
}
